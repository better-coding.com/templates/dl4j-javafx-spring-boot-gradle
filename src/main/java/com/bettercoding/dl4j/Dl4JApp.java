package com.bettercoding.dl4j;

import com.bettercoding.dl4j.controller.LearningGuiController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Dl4JApp extends Application {
    private ConfigurableApplicationContext springContext;
    private Parent rootNode;
    private FXMLLoader fxmlLoader;

    public static void main(String[] args) {
//        CudaEnvironment.getInstance().getConfiguration().allowMultiGPU(true);
        launch(args);
    }

    @Override
    public void init() {
        springContext = SpringApplication.run(Dl4JApp.class);
        fxmlLoader = new FXMLLoader();
        fxmlLoader.setControllerFactory(springContext::getBean);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        fxmlLoader.setLocation(getClass().getResource("/fxml/learning-gui.fxml"));
        rootNode = fxmlLoader.load();
        LearningGuiController abstractLearningGuiController = fxmlLoader.getController();

        primaryStage.setTitle("Better-Coding,com - Deep Learning Tutorial");
        primaryStage.setResizable(false);
        Scene scene = new Scene(rootNode);
        primaryStage.setScene(scene);
        primaryStage.sizeToScene();
        primaryStage.setOnCloseRequest(event -> abstractLearningGuiController.onCloseRequest());
        primaryStage.show();
    }

    @Override
    public void stop() {
        springContext.stop();
    }
}